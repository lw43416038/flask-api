class Config(object):
    """工程的配置信息"""
    SECRET_KEY = "xhosido*F(DHSDF*D(SDdslfhdos"

    # 数据库的配置信息 mysql
    SQLALCHEMY_DATABASE_URI = "mysql://spider:safe2SE%s@106.14.37.208:3306/spider_data"
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    # SQLALCHEMY_ECHO=True
    JSON_AS_ASCII = False




class DevelopmentConfig(Config):
    """开发模式使用的配置信息"""
    DEBUG = True



class ProductionConfig(Config):
    """生产模式 线上模式的配置信息"""
    pass


config_dict = {
    "develop": DevelopmentConfig,
    "product": ProductionConfig
}