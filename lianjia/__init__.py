from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect
from config import config_dict
from lianjia.utils.commons import RegexConverter
import logging
from logging.handlers import RotatingFileHandler

db = SQLAlchemy()
csrf = CSRFProtect()

logging.basicConfig(level=logging.DEBUG)  # 调试debug级
# 创建日志记录器，指明日志保存的路径、每个日志文件的最大大小、保存的日志文件个数上限
file_log_handler = RotatingFileHandler("logs/log", maxBytes=1024 * 1024 * 100, backupCount=10,encoding='utf-8')
# 创建日志记录的格式                 日志等级    输入日志信息的文件名 行数    日志信息
formatter = logging.Formatter('%(asctime)s %(levelname)s %(filename)s line:%(lineno)d %(thread)d %(message)s')
# 为刚创建的日志记录器设置日志记录格式
file_log_handler.setFormatter(formatter)
# 为全局的日志工具对象（flask app使用的）添加日记录器
logging.getLogger().addHandler(file_log_handler)


def create_app(config_name):
    app = Flask(__name__)
    conf = config_dict[config_name]
    # 设置falsk的配置信息
    app.config.from_object(conf)

    # 初始化数据库db
    db.init_app(app)

    # 初始化 CRSF
    # csrf.init_app(app)

    # 向app中添加自定义的路由转换器
    app.url_map.converters["re"] = RegexConverter

    # 注册蓝图
    from .api_1_0 import api

    app.register_blueprint(api, url_prefix="/api/v1_0")

    return app
