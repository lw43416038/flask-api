from . import api
from lianjia.models import House, Area, HouseImg, City
from lianjia.utils.response_code import RET
from flask import jsonify, current_app, request
import json
from sqlalchemy import or_
from sqlalchemy import and_, func


# @api.route("/city_house",methods=["GET","POST"])
# def city_hosue():
#     # 城市房屋首页展示接口
#     # city_name=request.args.get("city_name")
#     # page = request.args.get("page")
#     if request.method =="GET":
#         city_name = request.args.get("city_name")
#         page = request.args.get("page")
#     else:
#         city_name = request.form.get("city_name")
#         page = request.form.get("page")
#     if page:
#         if page.isdigit()and int(page)>0:
#             page = int(page)
#         else:
#             return jsonify(status=RET.PARAMERR, desc="参数错误")
#     else:
#         page = 1
#     if not city_name:
#         return jsonify(status=RET.PARAMERR, desc="参数错误")
#     try:
#
#         houses_query = House.query.filter_by(city=city_name,is_delete=0).order_by(House.service_type)
#
#     except Exception as e:
#         current_app.logger.error(e)
#         return jsonify(status=RET.DBERR, desc="系统繁忙")
#     houses_query_page=houses_query.paginate(page, 20, False)
#     houses_query_li=houses_query_page.items
#     total_page = houses_query_page.pages
#     num = houses_query_page.total
#     hot_houses_info = []
#     for i in houses_query_li:
#         hot_houses_info.append(i.to_basic_dict())
#
#     resp = dict(status=RET.OK,desc="查询成功", result={"hot_house_info": hot_houses_info,"total_page":total_page,"current_page":page,"num":num})
#     return jsonify(resp), 200, {"Content-Type": "application/json"}


# /api/v1_0/check_house?city_name=郑州&keyword=恒大
# @api.route("/check_house",methods=["GET","POST"])
# def check_hosue():
#     # 房屋查询接口
#
#     if request.method =="GET":
#         city = request.args.get("city_name")
#         keyword = request.args.get("keyword")
#         page = request.args.get("page")
#     else:
#         city = request.form.get("city_name")
#         keyword = request.form.get("keyword")
#         page = request.form.get("page")
#
#     if page:
#         if page.isdigit()and int(page)>0:
#             page = int(page)
#         else:
#             return jsonify(status=RET.PARAMERR, desc="参数错误")
#     else:
#         page = 1
#     if not all([city,keyword]):
#         return jsonify(status=RET.PARAMERR,desc="参数错误")
#
#     # keyword = parse.unquote(keyword)
#     try:
#         houses_query = House.query.filter(House.city == city, House.is_delete==0,House.house_name.like("%{}%".format(keyword)))
#     except Exception as e:
#         current_app.logger.error(e)
#         return jsonify(status=RET.DBERR, desc="系统繁忙")
#
#     houses_query_page = houses_query.paginate(page, 20, False)
#     houses_query_li = houses_query_page.items
#     total_page = houses_query_page.pages
#     num = houses_query_page.total
#     houses_info = []
#     for i in houses_query_li:
#         houses_info.append(i.to_basic_dict())
#     resp = dict(status=RET.OK,desc="查询成功", result={"houses_info": houses_info,"total_page":total_page,"current_page":page,"num":num})
#
#     return jsonify(resp), 200, {"Content-Type": "application/json"}


# /api/v1_0/areas?city_name=北京
@api.route("/areas", methods=["GET", "POST"])
def get_area():
    # 区域查询接口
    # city_name = request.args.get("city_name", "")
    if request.method == "GET":
        city_name = request.args.get("city_name")
    else:
        city_name = request.form.get("city_name")
    if not city_name:
        return jsonify(status="1002", desc="参数缺失,请输入参数city_name")
    city_obj = City.query.filter_by(city=city_name).first()
    if not city_obj:
        return jsonify(status=RET.QUERYERR, desc="暂无相关数据")
    try:
        areas = Area.query.filter_by(city_id=city_obj.id).all()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="网络异常,系统繁忙")
    areas_info = []
    for i in areas:
        areas_info.append({"area_name": i.area_name})

    resp = dict(status=RET.OK, desc="查询成功", result={"areas_info": areas_info})
    return jsonify(resp), 200, {"Content-Type": "application/json"}


# /api/v1_0/house_constraint?city_name=杭州&area=西湖;拱墅&price=[0,10000]&service_type=住宅&house_shape=3室&sale_status=售罄&keyword=""
@api.route("/house_constraint", methods=["GET", "POST"])
def get_house():
    # 条件查询接口
    city = request.values.get("city_name")

    if not city:
        return jsonify(status="1002", desc="参数缺失,请输入参数city_name")

    city_obj = City.query.filter_by(city=city).first()
    if not city_obj:
        return jsonify(status=RET.QUERYERR, desc="暂无相关数据")
    try:
        areas = Area.query.filter_by(city_id=city_obj.id).all()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="网络异常,系统繁忙")
    areas_info = []
    for i in areas:
        areas_info.append(i.area_name)

    a_params1, filter_params1 = [], []
    for area in areas_info:
        a_params1.append(House.area == area)
    filter_params1.append(or_(*a_params1))
    houses_query1 = House.query.filter(*filter_params1)
    total_num = houses_query1.paginate(1, 20, False).total

    area = request.values.get("area")
    service_type = request.values.get("service_type")
    house_shape = request.values.get("house_shape", "")
    sale_status = request.values.get("sale_status")
    price = request.values.get("price")
    keyword = request.values.get("keyword")
    page = request.values.get("page")
    if page:
        if page.isdigit() and int(page) > 0:
            page = int(page)
        else:
            return jsonify(status="1005", desc="参数错误,请输入正确的参数page")
    else:
        page = 1
    filter_params = []

    if area:
        area_list = area.split(";")
        a_params = []
        for area in area_list:
            a_params.append(House.area == area)
        filter_params.append(or_(*a_params))
    else:
        city_obj = City.query.filter_by(city=city).first()
        if not city_obj:
            return jsonify(status=RET.QUERYERR, desc="暂无相关数据")
        try:
            areas = Area.query.filter_by(city_id=city_obj.id).all()
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(status=RET.DBERR, desc="网络异常,系统繁忙")
        areas_info = []
        for i in areas:
            areas_info.append(i.area_name)

        a_params = []
        for area in areas_info:
            a_params.append(House.area == area)
        filter_params.append(or_(*a_params))

    if service_type:
        service_type_list = service_type.split(";")
        s_params = []
        for i in service_type_list:
            s_params.append(House.service_type == i)
        filter_params.append(or_(*s_params))
    if sale_status:
        sale_status_list = sale_status.split(";")
        sa_params = []
        for i in sale_status_list:
            sa_params.append(House.sale_status == i)
        filter_params.append(or_(*sa_params))
    if house_shape:
        house_shape_list = house_shape.split(";")
        h_params = []
        for i in house_shape_list:
            if i == "5室及以上":
                for j in range(5, 30):
                    h_params.append(House.house_shape.like("%{}%".format(j)))
            else:
                h_params.append(House.house_shape.like("%{}%".format(i)))
        filter_params.append(or_(*h_params))
    if keyword:
        filter_params.append(House.house_name.like("%{}%".format(keyword)))
    filter_params.append(House.city == city)
    filter_params.append(House.is_delete == 0)

    if price:

        try:
            price_list = price.split(";")
            p_params = []
            for price in price_list:
                price = json.loads(price)
                for i in price:
                    if not isinstance(i, int):
                        return jsonify(status="1006", desc="参数错误,请输入正确的参数price")
                if len(price) == 2:
                    price1 = price[0]
                    price2 = price[1]
                    p_params.append(and_(House.price >= price1, House.price <= price2))

                else:
                    price2 = price[0]
                    p_params.append(House.price >= price2)

            filter_params.append(or_(*p_params))
        except:
            return jsonify(status="1006", desc="参数错误,请输入正确的参数price")

    try:
        houses_query = House.query.filter(*filter_params).filter(
            House.sale_status.in_(("在售", "未开盘", "售罄", "下期待开"))).order_by(
            func.field(House.sale_status, *("在售", "未开盘", "售罄", "下期待开")))
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="网络异常,系统繁忙")
    houses_query_page = houses_query.paginate(page, 20, False)
    houses_query_li = houses_query_page.items
    total_page = houses_query_page.pages
    if total_page == 0:
        return jsonify(status=RET.QUERYERR, desc="暂无相关数据")
    if page > total_page:
        return jsonify(status="1005", desc="参数错误,请输入正确的参数page")

    num = houses_query_page.total
    houses_info = []
    for i in houses_query_li:
        houses_info.append(i.to_basic_dict())

    resp = dict(status=RET.OK, desc="查询成功",
                result={"houses_info": houses_info, "total_page": total_page, "current_page": page, "num": num,
                        "total_num": total_num})

    return jsonify(resp), 200, {"Content-Type": "application/json"}


# /api/v1_0/house_detaile?house_id=8253
@api.route("/house_detaile", methods=["GET", "POST"])
def get_house_detaile():
    # 房屋详情接口
    # house_id = request.args.get("house_id","")
    if request.method == "GET":
        house_id = request.args.get("house_id")
    else:
        house_id = request.form.get("house_id")
    if not house_id:
        return jsonify(status="1001", desc="参数缺失,请输入参数house_id")
    try:
        house = House.query.filter_by(id=house_id, is_delete=0).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="网络异常,系统繁忙")
    if not house:
        return jsonify(status=RET.QUERYERR, desc="暂无相关数据")
    house_info = house.to_full_dict()
    resp = dict(status=RET.OK, desc="查询成功", result={"house_info": house_info})
    return jsonify(resp), 200, {"Content-Type": "application/json"}

# /api/v1_0/house_album?house_id=8253
# @api.route("house_album",methods=["GET","POST"])
# def get_house_album():
#     # 房屋样板图接口
#     house_id = request.args.get("house_id", "")
#     if  not house_id.isdigit():
#         return jsonify(status=RET.PARAMERR,desc="参数错误")
#     try:
#         house = House.query.filter_by(id=house_id, is_delete=0).first()
#         house_img = HouseImg.query.filter_by(city=house.city,house_name=house.house_name,service_type=house.service_type ,img_type="样板间",is_delete=0).first()
#     except Exception as e:
#         current_app.logger.error(e)
#         return jsonify(status=RET.DBERR, desc="系统繁忙")
#
#     resp = dict(status=RET.OK, desc="查询成功",result={"house_album": house_img.img_url.split(",") if house_img else None})
#
#     return jsonify(resp), 200, {"Content-Type": "application/json"}
