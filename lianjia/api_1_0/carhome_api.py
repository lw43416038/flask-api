from . import api
from lianjia.models import CarDetaile, CarInfo, EnergyDetaile
from lianjia.utils.response_code import RET
from flask import jsonify, current_app, request


@api.route("/carinfo", methods=["GET", "POST"])
def get_carinfo():
    try:
        carinfo_query = CarInfo.query
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="系统繁忙")
    # page = request.args.get("page")
    if request.method == "GET":
        page = request.args.get("page")
    else:
        page = request.form.get("page")
    if page:
        if page.isdigit() and int(page) > 0:
            page = int(page)
        else:
            return jsonify(status=RET.PARAMERR, desc="参数错误,请输入正确的参数page")
    else:
        page = 1
    carinfos = []
    carinfo_page = carinfo_query.paginate(page, 10, False)
    carinfo_li = carinfo_page.items  # 当前页中的数据结果
    if not carinfo_li:
        return jsonify(status=RET.PARAMERR, desc="参数错误,请输入正确的参数page")
    total_page = carinfo_page.pages
    for i in carinfo_li:
        carinfos.append(i.to_full_dict())
    resp = dict(status=RET.OK, desc="查询成功",
                result={"carinfos": carinfos, "total_page": total_page, "current_page": page})

    return jsonify(resp), 200, {"Content-Type": "application/json"}


@api.route("/car_detaile", methods=["GET", "POST"])
def get_car_detaile():
    id = request.values.get("cid")
    if not id:
        return jsonify(status="1001", desc="参数缺失,请输入参数cid")
    else:
        if not id.isdigit():
            return jsonify(status="1002", desc="参数错误,请输入正确的cid")

    obj_info = CarInfo.query.filter_by(id=id).first()

    if not obj_info:
        return jsonify(status="1002", desc="参数错误,请输入正确的cid")
    try:
        obj_detail = CarDetaile.query.filter_by(brand_name=obj_info.brand_name, system_name=obj_info.system_name,
                                                car_name=obj_info.car_name).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="系统繁忙")
    if obj_detail:
        car_detailes = obj_detail.to_full_dict()

    else:
        try:
            obj_ene = EnergyDetaile.query.filter_by(brand_name=obj_info.brand_name, system_name=obj_info.system_name,
                                                    car_name=obj_info.car_name).first()
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(status=RET.DBERR, desc="系统繁忙")
        car_detailes = obj_ene.to_full_dict()

    resp = dict(status=RET.OK, desc="查询成功",
                result={"car_detailes": car_detailes})

    return jsonify(resp), 200, {"Content-Type": "application/json"}
