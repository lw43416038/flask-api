from . import api
from lianjia.models import QiuBaiWord, QiuBaiImg
from lianjia.utils.response_code import RET
from flask import jsonify, current_app, request
from datetime import date


@api.route("/qiubai/word", methods=["GET", "POST"])
def get_qiubai_word():
    try:
        word_query = QiuBaiWord.query
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="系统繁忙")

    if request.method =="GET":
        page = request.args.get("page")
    else:
        page = request.form.get("page")
    if page:
        if page.isdigit() and int(page) > 0:
            page = int(page)
        else:
            return jsonify(status=RET.PARAMERR, desc="参数错误,请输入正确的参数page")
    else:
        page = 1
    qiubai_word_page = word_query.paginate(page, 10, False)
    qiubai_word_li = qiubai_word_page.items  # 当前页中的数据结果
    if not qiubai_word_li:
        return jsonify(status=RET.PARAMERR, desc="参数错误,请输入正确的参数page")
    total_page = qiubai_word_page.pages
    words_joke = []
    for i in qiubai_word_li:
        words_joke.append({"id": i.id, "content": i.content})
    resp = dict(status=RET.OK, desc="查询成功",
                result={"words_joke": words_joke, "total_page": total_page, "current_page": page})

    return jsonify(resp), 200, {"Content-Type": "application/json"}

@api.route("/qiubai/img", methods=["GET", "POST"])
def get_qiubai_img():
    try:
        img_query = QiuBaiImg.query
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="系统繁忙")
    if request.method =="GET":
        page = request.args.get("page")
    else:
        page = request.form.get("page")
    if page:
        if page.isdigit() and int(page) > 0:
            page = int(page)
        else:
            return jsonify(status=RET.PARAMERR, desc="参数错误,请输入正确的参数page")
    else:
        page = 1

    qiubai_img_page = img_query.paginate(page, 10, False)
    qiubai_img_li = qiubai_img_page.items  # 当前页中的数据结果
    if not qiubai_img_li:
        return jsonify(status=RET.PARAMERR, desc="参数错误,请输入正确的参数page")
    total_page = qiubai_img_page.pages
    imgs_joke = []
    for i in qiubai_img_li:
        imgs_joke.append({"id": i.id, "content": i.content,"img_url":"https:"+i.img})
    resp = dict(status=RET.OK, desc="查询成功",
                result={"imgs_joke": imgs_joke, "total_page": total_page, "current_page": page})

    return jsonify(resp), 200, {"Content-Type": "application/json"}
