from . import api
from lianjia.models import GoldModel
from lianjia.utils.response_code import RET
from flask import jsonify, current_app, request
from datetime import date


@api.route("/daily", methods=["GET", "POST"])
def get_daily():
    try:
        gold_query = GoldModel.query.order_by(GoldModel.publish_time.desc())
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="系统繁忙")
    if request.method =="GET":
        page = request.args.get("page")
    else:
        page = request.form.get("page")
    if page:
        page=int(page)
    else:
        page=1
    daily_page = gold_query.paginate(page, 10, False)
    daily_li = daily_page.items  # 当前页中的数据结果
    total_page = daily_page.pages
    if page > total_page:
        return jsonify(status="1001", desc="参数错误,请输入正确的参数page")
    dailyinfos = []
    for i in daily_li:
        dailyinfos.append(i.to_part_dict())
    resp = dict(status=RET.OK, desc="查询成功",
                result={"dailyinfos": dailyinfos, "total_page": total_page, "current_page": page})

    return jsonify(resp), 200, {"Content-Type": "application/json"}


@api.route("/daily_detaile", methods=["GET", "POST"])
def get_daily_detaile():
    try:
        gold_query = GoldModel.query.order_by(GoldModel.publish_time.desc())
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="系统繁忙")
    if request.method =="GET":
        page = request.args.get("page")
    else:
        page = request.form.get("page")
    if page:
        page = int(page)
    else:
        page = 1
    daily_detaile_page = gold_query.paginate(page, 10, False)
    daily_detaile_li = daily_detaile_page.items  # 当前页中的数据结果
    total_page = daily_detaile_page.pages
    if page > total_page:
        return jsonify(status="1001", desc="参数错误,请输入正确的参数page")
    detailes = []
    for i in daily_detaile_li:
        detailes.append(i.to_full_dict())
    resp = dict(status=RET.OK, desc="查询成功",
                result={"detailes": detailes, "total_page": total_page, "current_page": page})

    return jsonify(resp), 200, {"Content-Type": "application/json"}
