from . import api
from lianjia.models import ShiShiModel, SingleModel, ElseModel
from lianjia.utils.response_code import RET
from flask import jsonify, current_app, request
from datetime import date, datetime, timedelta
import re, json, requests


@api.route("/piaofang/shishi", methods=["GET", "POST"])
def get_shishi():
    strat_time = request.values.get("datetime")
    if strat_time:
        dtime = strat_time[:10]
        try:
            dtime = date(int(dtime[:4]), int(dtime[5:7]), int(dtime[8:]))
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(status=RET.PARAMERR, desc="参数错误,请输入正确的参数datetime")
        min = strat_time[11:]
        res = re.match(r'([1]*\d|[2][0-4]|\d):[0-5]\d$', min)
        if not res:
            return jsonify(status=RET.PARAMERR, desc="参数错误,请输入正确的参数datetime")
    else:
        dtime = date.today()
        min = datetime.now().strftime('%H:%M')

    if dtime > date.today():
        return jsonify(status=RET.PARAMERR, desc="参数错误,请输入正确的参数datetime")

    filter_params = list()
    filter_params.append(ShiShiModel.datetime == dtime)

    if min[-1:] == "5" or min[-1:] == "0":
        pass
    elif int(min[-1:]) > 5:
        min = min[:-1] + "5"
    else:
        min = min[:-1] + "0"

    filter_params.append(ShiShiModel.update_time == min)
    query = ShiShiModel.query.filter(*filter_params).all()

    queryinfo = list()
    for i in query:
        queryinfo.append(i.to_base_dict())
    resp = dict(status=RET.OK, desc="查询成功",
                result={"queryinfo": queryinfo})

    return jsonify(resp), 200, {"Content-Type": "application/json"}


@api.route("/piaofang/day", methods=["GET", "POST"])
def get_singleday():
    if request.method == "GET":
        dtime = request.args.get("date", date.today())

    else:
        dtime = request.form.get("date", date.today())

    if isinstance(dtime, str):
        try:
            dtime = date(int(dtime[:4]), int(dtime[5:7]), int(dtime[8:]))
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(status="2002", desc="参数错误,请输入正确的参数date")

    if dtime > date.today():
        return jsonify(status="2002", desc="参数错误,请输入正确的参数date")

    elif dtime < date.today():
        query = SingleModel.query.filter_by(piaofang_date=str(dtime)).all()

    else:
        dtime = date.today() - timedelta(days=1)
        query = SingleModel.query.filter_by(piaofang_date=str(dtime)).all()
    queryinfo = list()
    for i in query:
        queryinfo.append(i.to_full_dict())
    resp = dict(status=RET.OK, desc="查询成功",
                result={"queryinfo": queryinfo, "piaofang_date": str(dtime)})

    return jsonify(resp), 200, {"Content-Type": "application/json"}


def getWeekFirstday(year_str, week_str):
    # 输入第几周返回当周的星期1的日期
    # year_str = weekflag[0:4]  # 取到年份
    # week_str = weekflag[5:]  # 取到周
    if int(week_str) >= 53:
        Monday = "Error,Week Num greater than 53!"
    else:
        yearstart_str = year_str + '0101'  # 当年第一天
        yearstart = datetime.strptime(yearstart_str, '%Y%m%d')  # 格式化为日期格式
        yearstartcalendarmsg = yearstart.isocalendar()  # 当年第一天的周信息
        yearstartweekday = yearstartcalendarmsg[2]
        yearstartyear = yearstartcalendarmsg[0]
        if yearstartyear < int(year_str):
            daydelat = (8 - int(yearstartweekday)) + (int(week_str) - 1) * 7
        else:
            daydelat = (8 - int(yearstartweekday)) + (int(week_str) - 2) * 7
        Monday = (yearstart + timedelta(days=daydelat)).date()

    return Monday


def get_1st_of_last_month():
    """
    获取上个月第一天的日期，然后加21天就是22号的日期
    :return: 返回日期
    """
    today = date.today()
    year = today.year
    month = today.month
    if month == 1:
        month = 12
        year -= 1
    else:
        month -= 1
    res = date(year, month, 1)
    return res


@api.route("/piaofang/else", methods=["GET", "POST"])
def get_else():
    type = request.values.get("type")
    if not type:
        return jsonify(status="2001", desc="参数缺失,请输入参数type")
    dtime = request.values.get("date")

    if type not in ["周末", "单周", "单月", "年度", "全球"]:
        return jsonify(status="2002", desc="参数错误,请输入正确的参数type")

    if type == "单周":
        if not dtime:
            year_str = str(date.today().year)
            week_str = str(date.today().isocalendar()[1] - 1)
            Monday = getWeekFirstday(year_str, week_str)
            dtime = str(Monday) + "至" + str(Monday + timedelta(days=6))
        else:
            if not dtime.isdigit():
                return jsonify(status="2002", desc="参数错误,请输入正确的参数date")
            year_str = str(date.today().year)
            Monday = getWeekFirstday(year_str, dtime)
            dtime = str(Monday) + "至" + str(Monday + timedelta(days=6))
        query = ElseModel.query.filter_by(piaofang_date=dtime, type=type).first()
        queryinfo1 = json.loads(query.data)['data1'] if query else []
        queryinfo = []
        if queryinfo1:
            for i in queryinfo1:
                item = {"Amount_Up": i["Amount_Up"], "AvgPeople": i["AvgPeople"], "MovieDay": i["MovieDay"],
                        "MovieName": i["MovieName"], "MovieRank": i["MovieRank"], "Rank_Up": i["Rank_Up"],
                        "AvgPrice": i["AvgPrice"],
                        "WeekAmount": i["WeekAmount"], "WomIndex": i["WomIndex"], "SumWeekAmount": i["SumWeekAmount"]}
                queryinfo.append(item)

        resp = dict(status=RET.OK, desc="查询成功",
                    result={"queryinfo": queryinfo, "piaofang_date": dtime})

        return jsonify(resp), 200, {"Content-Type": "application/json"}

    elif type == "周末":
        if not dtime:
            year_str = str(date.today().year)
            week_str = str(date.today().isocalendar()[1] - 1)
            Monday = getWeekFirstday(year_str, week_str)
            dtime = str(Monday + timedelta(days=4)) + "至" + str(Monday + timedelta(days=6))
        else:
            if not dtime.isdigit():
                return jsonify(status="2002", desc="参数错误,请输入正确的参数date")
            year_str = str(date.today().year)
            Monday = getWeekFirstday(year_str, dtime)
            dtime = str(Monday + timedelta(days=4)) + "至" + str(Monday + timedelta(days=6))

        query = ElseModel.query.filter_by(piaofang_date=dtime, type=type).first()
        queryinfo1 = json.loads(query.data)['data1'] if query else []
        queryinfo = []
        if queryinfo1:
            for i in queryinfo1:
                item = {"AvgPrice": i["AvgPrice"], "AvpPeoPle": i["AvpPeoPle"], "BoxOffice": i["BoxOffice"],
                        "BoxOffice_Up": i["BoxOffice_Up"], "MovieDay": i["MovieDay"], "MovieName": i["MovieName"],
                        "MovieRank": i["MovieRank"],
                        "Rank_Up": i["Rank_Up"], "SumBoxOffice": i["SumBoxOffice"], "WomIndex": i["WomIndex"]}
                queryinfo.append(item)

        resp = dict(status=RET.OK, desc="查询成功",
                    result={"queryinfo": queryinfo, "piaofang_date": dtime})

        return jsonify(resp), 200, {"Content-Type": "application/json"}

    elif type == "单月":
        if not dtime:
            dtime = get_1st_of_last_month()
            dtime = dtime.strftime("%Y-%m")
            dtime = dtime.replace("-", "年") + "月"
        else:
            try:
                dtime = date(int(dtime[:4]), int(dtime[5:7]), 1)
                dtime = dtime.strftime("%Y-%m")
                dtime = dtime.replace("-", "年") + "月"
                month = int(dtime[5:7])
            except Exception as e:
                current_app.logger.error(e)
                return jsonify(status="2002", desc="参数错误,请输入正确的参数date")
            if month == date.today().month:
                resp = requests.get("http://139.196.108.4:9001/search/piaofang/singlemonth")

                return resp.content.decode(), 200, {"Content-Type": "application/json"}

        query = ElseModel.query.filter_by(piaofang_date=dtime, type=type).first()
        queryinfo1 = json.loads(query.data)['data1'] if query else []
        queryinfo = []
        if queryinfo1:
            for i in queryinfo1:
                item = {"Irank": i["Irank"], "MovieName": i["MovieName"], "WomIndex": i["WomIndex"],
                        "avgboxoffice": i["avgboxoffice"], "avgshowcount": i["avgshowcount"], "box_pro": i["box_pro"],
                        "boxoffice": i["boxoffice"],
                        "days": i["days"], "releaseTime": i["releaseTime"]}
                queryinfo.append(item)

        resp = dict(status=RET.OK, desc="查询成功",
                    result={"queryinfo": queryinfo, "piaofang_date": dtime})

        return jsonify(resp), 200, {"Content-Type": "application/json"}

    elif type == "年度":
        if not dtime:
            dtime = str(date.today().year - 1) + "年"
        else:
            try:
                dtime = date(int(dtime[:4]), 1, 1)
                dtime = str(dtime.year) + "年"
                year = int(dtime[:4])
            except Exception as e:
                current_app.logger.error(e)
                return jsonify(status="2002", desc="参数错误,请输入正确的参数date")
            if year == date.today().year:
                resp = requests.get("http://139.196.108.4:9001/search/piaofang/singleyear")
                return resp.content.decode(), 200, {"Content-Type": "application/json"}

        query = ElseModel.query.filter_by(piaofang_date=dtime, type=type).first()
        queryinfo = json.loads(query.data) if query else []
        resp = dict(status=RET.OK, desc="查询成功",
                    result={"queryinfo": queryinfo, "piaofang_date": dtime})

        return jsonify(resp), 200, {"Content-Type": "application/json"}

    else:
        if not dtime:
            year_str = str(date.today().year)
            week_str = str(date.today().isocalendar()[1] - 1)
            Monday = getWeekFirstday(year_str, week_str)
            dtime = str(Monday + timedelta(days=4)) + "至" + str(Monday + timedelta(days=6))
        else:
            if not dtime.isdigit():
                return jsonify(status="2002", desc="参数错误,请输入正确的参数date")
            year_str = str(date.today().year)
            Monday = getWeekFirstday(year_str, dtime)
            dtime = str(Monday + timedelta(days=4)) + "至" + str(Monday + timedelta(days=6))

        query = ElseModel.query.filter_by(type=type, piaofang_date=dtime).first()

        queryinfo1 = json.loads(query.data) if query else []
        queryinfo = []
        for i in queryinfo1:
            queryinfo.append({"Area": i["Area"], "BoxOffice": i["BoxOffice"], "BoxOffice_Pro": i["BoxOffice_Pro"],
                              "MovieName": i["MovieName"], "Rank": i["Rank"], "RankChange": i["RankChange"],
                              "SumBoxOffice": i["SumBoxOffice"], "WeekNum": i["WeekNum"]})
        resp = dict(status=RET.OK, desc="查询成功",
                    result={"queryinfo": queryinfo, "piaofang_date": dtime})

        return jsonify(resp), 200, {"Content-Type": "application/json"}
