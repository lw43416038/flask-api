from . import api
from lianjia.models import ExchangeRateModel
from lianjia.utils.response_code import RET
from flask import jsonify, current_app



@api.route("/huilv", methods=["GET", "POST"])
def get_huilv():
    try:
        rate_query = ExchangeRateModel.query.filter_by(is_delete=0).all()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="系统繁忙")
    rates = []
    for i in rate_query:
        rates.append(i.to_full_dict())
    resp = dict(status=RET.OK, desc="查询成功",
                result={"rates": rates})

    return jsonify(resp), 200, {"Content-Type": "application/json"}


@api.route("/huilv_name", methods=["GET", "POST"])
def get_huilv_name():
    try:
        rate_query = ExchangeRateModel.query.filter_by(is_delete=0).all()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="系统繁忙")
    names = []
    for i in rate_query:
        if i.currency not in names:
            names.append(i.currency)

    resp = dict(status=RET.OK, desc="查询成功",
                result={"names": names})

    return jsonify(resp), 200, {"Content-Type": "application/json"}
