from . import api
from lianjia.models import YouJia
from lianjia.utils.response_code import RET
from flask import jsonify, current_app, request
from datetime import date


@api.route("/youjia", methods=["GET", "POST"])
def get_youjia():
    try:
        youjia_query = YouJia.query.filter_by(is_delete=0).all()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="系统繁忙")
    prices = []
    for i in youjia_query:
        prices.append(i.to_full_dict())
    resp = dict(status=RET.OK, desc="查询成功",
                result={"prices": prices})

    return jsonify(resp), 200, {"Content-Type": "application/json"}


@api.route("/youjia_area", methods=["GET", "POST"])
def get_youjia_area():
    try:
        youjia_query = YouJia.query.filter_by(is_delete=0).all()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="系统繁忙")
    areas = []
    for i in youjia_query:
        areas.append(i.area)

    resp = dict(status=RET.OK, desc="查询成功",
                result={"areas": areas})

    return jsonify(resp), 200, {"Content-Type": "application/json"}
