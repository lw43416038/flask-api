from . import api
from lianjia.models import HouseShape1, HouseImg, HouseDynamic, House, HouseInfo1, HouseCertificate1
from lianjia.utils.response_code import RET
from flask import jsonify, current_app, request
import math


@api.route("/house_img", methods=["GET", "POST"])
def house_img():
    # 楼盘相册接口
    if request.method == "GET":
        house_id = request.args.get("house_id")
        img_type = request.args.get("type")
        page = request.args.get("page")
    else:
        house_id = request.form.get("house_id")
        img_type = request.form.get("type")
        page = request.form.get("page")
    if page:
        if page.isdigit() and int(page) > 0:
            page = int(page)
        else:
            return jsonify(status="1005", desc="参数错误,请输入正确的参数page")
    else:
        page = 1

    if not house_id:
        return jsonify(status="1001", desc="参数缺失,请输入参数house_id")
    if not img_type:
        return jsonify(status="1003", desc="参数缺失,请输入参数type")

    try:
        house = House.query.filter_by(id=house_id).first()
        if not house:
            return jsonify(status=RET.QUERYERR, desc="暂无相关数据")
        house_img = HouseImg.query.filter_by(city=house.city, house_name=house.house_name,
                                             service_type=house.service_type, img_type=img_type, is_delete=0,
                                             detail_url=house.detail_url).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="网络异常,系统繁忙")
    if not house_img:
        return jsonify(status=RET.QUERYERR, desc="暂无相关数据")
    house_imgs = house_img.img_url.split(",")

    total_page = math.ceil(len(house_imgs) / 14)
    if page > total_page:
        return jsonify(status="1005", desc="参数错误,请输入正确的参数page")
    num = len(house_imgs)
    new_list = [house_imgs[i:i + 14] for i in range(0, len(house_imgs), 14)]
    try:
        house_imgs = new_list[page - 1]
        house_imgs = [i.replace("235x178", "339x280") for i in house_imgs]
    except:
        house_imgs = []

    resp = dict(status=RET.OK, desc="查询成功",
                result={"house_imgs": house_imgs, "total_page": total_page, "current_page": page, "num": num})

    return jsonify(resp), 200, {"Content-Type": "application/json"}


@api.route("/img_type", methods=["GET", "POST"])
def get_img_type():
    if request.method == "GET":
        house_id = request.args.get("house_id")
    else:
        house_id = request.form.get("house_id")
    if not house_id:
        return jsonify(status="1001", desc="参数缺失,请输入参数house_id")
    try:
        house = House.query.filter_by(id=house_id).first()
        if not house:
            return jsonify(status=RET.QUERYERR, desc="暂无相关数据")
        house_img_query = HouseImg.query.filter_by(city=house.city, house_name=house.house_name,
                                                   service_type=house.service_type, is_delete=0,
                                                   detail_url=house.detail_url).all()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="网络异常,系统繁忙")
    img_types = []
    for i in house_img_query:
        img_types.append({"total_num": i.img_num, "type_name": i.img_type,
                          "back_img": i.img_url.split(",")[0].replace("235x178", "339x280")})

    resp = dict(status=RET.OK, desc="查询成功", result={"img_types": img_types})
    return jsonify(resp), 200, {"Content-Type": "application/json"}


@api.route("/house_shape", methods=["GET", "POST"])
def house_shape():
    # 户型介绍接口
    # house_id = request.args.get("house_id")
    if request.method == "GET":
        house_id = request.args.get("house_id")
    else:
        house_id = request.form.get("house_id")
    if not house_id:
        return jsonify(status="1001", desc="参数缺失,请输入参数house_id")
    try:
        house = House.query.filter_by(id=house_id).first()
        if not house:
            return jsonify(status=RET.QUERYERR, desc="暂无相关数据")
        house_shape_query = HouseShape1.query.filter_by(city=house.city, house_name=house.house_name,
                                                        service_type=house.service_type, is_delete=0,
                                                        detail_url=house.detail_url).all()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="网络异常,系统繁忙")
    house_shapes = []
    for i in house_shape_query:
        house_shapes.append({"house_shape_name": i.house_shape_name,
                             "house_type": i.house_type, "avg_price": "".join(i.avg_price.split(",")[1:3]),
                             "house_shape_feature": i.house_shape_feature.replace(",,", " ").replace(",", "").split(
                                 " "),
                             "house_describe": i.house_describe.replace('\n', ''),
                             })

    resp = dict(status=RET.OK, desc="查询成功", result={"house_shapes": house_shapes})

    return jsonify(resp), 200, {"Content-Type": "application/json"}


@api.route("/house_info", methods=["GET", "POST"])
def house_info():
    # 开盘信息接口
    # house_id = request.args.get("house_id")
    if request.method == "GET":
        house_id = request.args.get("house_id")
    else:
        house_id = request.form.get("house_id")
    if not house_id:
        return jsonify(status="1001", desc="参数缺失,请输入参数house_id")
    try:
        house = House.query.filter_by(id=house_id).first()
        if not house:
            return jsonify(status=RET.QUERYERR, desc="暂无相关数据")
        house_info_query = HouseInfo1.query.filter_by(city=house.city, house_name=house.house_name,
                                                      service_type=house.service_type, is_delete=0,
                                                      detail_url=house.detail_url).all()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="网络异常,系统繁忙")

    house_infos = []
    for i in house_info_query:
        house_infos.append({"new_open_time": i.new_open_time, "sale_num": i.sale_num,
                            "done_house_time": i.done_house_time})

    resp = dict(status=RET.OK, desc="查询成功", result={"house_infos": house_infos})

    return jsonify(resp), 200, {"Content-Type": "application/json"}


@api.route("/house_cert", methods=["GET", "POST"])
def house_cert():
    # 预售许可证接口
    # house_id = request.args.get("house_id")
    if request.method == "GET":
        house_id = request.args.get("house_id")
    else:
        house_id = request.form.get("house_id")
    if not house_id:
        return jsonify(status="1001", desc="参数缺失,请输入参数house_id")
    try:
        house = House.query.filter_by(id=house_id).first()
        if not house:
            return jsonify(status=RET.QUERYERR, desc="暂无相关数据")
        house_cert_query = HouseCertificate1.query.filter_by(city=house.city, house_name=house.house_name,
                                                             service_type=house.service_type, is_delete=0,
                                                             detail_url=house.detail_url).all()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="网络异常,系统繁忙")
    house_certs = []
    for i in house_cert_query:
        house_certs.append(
            {"licence": i.licence, "bind_floor": i.bind_floor, "licence_time": i.licence_time})
    resp = dict(status=RET.OK, desc="查询成功", result={"house_certs": house_certs})
    return jsonify(resp), 200, {"Content-Type": "application/json"}


@api.route("/house_plan", methods=["GET", "POST"])
def house_plan():
    # 规划信息接口
    # house_id = request.args.get("house_id")
    if request.method == "GET":
        house_id = request.args.get("house_id")
    else:
        house_id = request.form.get("house_id")
    if not house_id:
        return jsonify(status="1001", desc="参数缺失,请输入参数house_id")
    try:
        house = House.query.filter_by(id=house_id).first()

    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="网络异常,系统繁忙")
    if not house:
        return jsonify(status=RET.QUERYERR, desc="暂无相关数据")
    resp = dict(status=RET.OK, desc="查询成功",
                result={"build_type": house.build_type, "total_space": house.total_space,
                        "build_space": house.build_space, "total_floors": house.total_floors,
                        "house_years": house.house_years, "greening_rate": house.greening_rate,
                        "volume_rate": house.volume_rate, "business_type": house.service_type, })
    return jsonify(resp), 200, {"Content-Type": "application/json"}


@api.route("/house_facility", methods=["GET", "POST"])
def house_facility():
    # 配套信息接口
    # house_id = request.args.get("house_id")
    if request.method == "GET":
        house_id = request.args.get("house_id")
    else:
        house_id = request.form.get("house_id")
    if not house_id:
        return jsonify(status="1001", desc="参数缺失,请输入参数house_id")
    try:
        house = House.query.filter_by(id=house_id).first()

    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="网络异常,系统繁忙")
    if not house:
        return jsonify(status=RET.QUERYERR, desc="暂无相关数据")
    resp = dict(status=RET.OK, desc="查询成功",
                result={"property_company": house.property_company, "property_fee": house.property_fee,
                        "water_mode": house.water_mode,
                        "parking_lot": ";".join([i.strip() for i in house.parking_lot.split("；")]),
                        "parking_ratio": house.parking_ratio,
                        "heating_mode": house.heating_mode, "power_supply_mode": house.power_supply_mode})

    return jsonify(resp), 200, {"Content-Type": "application/json"}


@api.route("/shape_img", methods=["GET", "POST"])
def get_shape_img():
    # house_id = request.args.get("house_id")
    # house_type = request.args.get("house_type")
    if request.method == "GET":
        house_id = request.args.get("house_id")
        house_type = request.args.get("house_type")
    else:
        house_id = request.form.get("house_id")
        house_type = request.form.get("house_type")

    if not house_id:
        return jsonify(status="1001", desc="参数缺失,请输入参数house_id")
    if not house_type:
        return jsonify(status="1004", desc="参数缺失,请输入参数house_type")

    try:
        house = House.query.filter_by(id=house_id).first()
        if not house:
            return jsonify(status=RET.QUERYERR, desc="暂无相关数据")
        house_shape_query = HouseShape1.query.filter_by(city=house.city, house_name=house.house_name,
                                                        service_type=house.service_type, house_type=house_type,
                                                        is_delete=0, detail_url=house.detail_url).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(status=RET.DBERR, desc="网络异常,系统繁忙")
    if not house_shape_query:
        return jsonify(status=RET.QUERYERR, desc="暂无相关数据")
    shape_img = house_shape_query.house_shape_img.replace("140x115", "1000x")

    resp = dict(status=RET.OK, desc="查询成功",
                result={"shape_img": shape_img})
    return jsonify(resp, ), 200, {"Content-Type": "application/json"}
