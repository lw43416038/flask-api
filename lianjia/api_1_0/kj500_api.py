from . import api
from lianjia.models import *
from lianjia.utils.response_code import RET
from flask import jsonify, current_app, request
from datetime import date
from sqlalchemy import and_


@api.route("/kjindex", methods=["GET", "POST"])
def get_kjindex():
    try:
        obj_list = NationalDetail.query.filter_by(is_delete=0).all()
    except:
        return jsonify(status=RET.DBERR, desc="系统繁忙")
    data = []
    for i in obj_list:
        data.append(i.to_base_dict())

    resp = dict(status=RET.OK, desc="查询成功",
                result={"data": data})
    return jsonify(resp), 200, {"Content-Type": "application/json"}


@api.route("/kjdetail", methods=["GET", "POST"])
def get_kjdetail():
    type = request.values.get("type")
    if not type:
        return jsonify(status=RET.PARAMLOSS, desc="参数缺失,请输入参数type")
    try:
        obj = NationalDetail.query.filter_by(type=type, is_delete=0).first()
    except:
        return jsonify(status=RET.DBERR, desc="系统繁忙")
    if not obj:
        return jsonify(status=RET.PARAMERR, desc="参数错误,请输入正确的参数type")
    resp = dict(status=RET.OK, desc="查询成功",
                result=obj.to_detail_dict())
    return jsonify(resp), 200, {"Content-Type": "application/json"}


@api.route("/kjprevious", methods=["GET", "POST"])
def get_kjprevious():
    if request.method == "GET":
        type = request.args.get("type")
    else:
        type = request.form.get("type")
    if not type:
        return jsonify(status=RET.PARAMLOSS, desc="参数缺失,请输入参数type")
    try:
        obj = NationalDetail.query.filter_by(type=type, is_delete=2).first()
    except:
        return jsonify(status=RET.DBERR, desc="系统繁忙")
    if not obj:
        return jsonify(status=RET.PARAMERR, desc="参数错误,请输入正确的参数type")
    resp = dict(status=RET.OK, desc="查询成功",
                result=obj.to_previous_dict())
    return jsonify(resp), 200, {"Content-Type": "application/json"}


@api.route("/kjhistory", methods=["GET", "POST"])
def get_kjhistory():
    if request.method == "GET":
        type = request.args.get("type")
        page = request.args.get("page")
        start = request.args.get("s")
        end = request.args.get("e")
    else:
        type = request.form.get("type")
        page = request.form.get("page")
        start = request.form.get("s")
        end = request.form.get("e")

    filter_params = []
    if not type:
        return jsonify(status=RET.PARAMLOSS, desc="参数缺失,请输入参数type")
    if page:
        if page.isdigit() and int(page) > 0 and int(page) < 5:
            page = int(page)
        else:
            return jsonify(status=RET.PARAMERR, desc="参数错误,请输入正确的参数page")
    else:
        page = 1
    if start:
        if not start.isdigit():
            return jsonify(status="1007", desc="参数错误,请输入正确的参数s")
    if end:
        if not end.isdigit():
            return jsonify(status="1008", desc="参数错误,请输入正确的参数e")

    if type not in ["超级大乐透", "排列5", "排列3", "7星彩", "七乐彩", "福彩3D", "双色球"]:
        return jsonify(status=RET.PARAMERR, desc="参数错误,请输入正确的参数type")
    if type == "超级大乐透":
        first_cycle_num = DaLeTou.query.first().cycle_num
        last_cycle_num = DaLeTou.query.order_by(DaLeTou.cycle_num.desc()).first().cycle_num
        if start and end:
            if int(start) < int(first_cycle_num) or int(start) > int(last_cycle_num):
                return jsonify(status="1007", desc="参数错误,请输入正确的参数s")
            if int(end) > int(last_cycle_num) or int(end) < int(first_cycle_num):
                return jsonify(status="1008", desc="参数错误,请输入正确的参数e")
            if int(start) > int(end):
                return jsonify(status=RET.QUERYERR, desc="无相关数据")
            filter_params.append(and_(DaLeTou.cycle_num >= start, DaLeTou.cycle_num <= end))
        elif start:
            if int(start) < int(first_cycle_num) or int(start) > int(last_cycle_num):
                return jsonify(status="1007", desc="参数错误,请输入正确的参数s")
            filter_params.append(DaLeTou.cycle_num >= start)
        elif end:
            if int(end) > int(last_cycle_num) or int(end) < int(first_cycle_num):
                return jsonify(status="1008", desc="参数错误,请输入正确的参数e")
            filter_params.append(DaLeTou.cycle_num <= end)
        try:
            obj_query = DaLeTou.query.filter(*filter_params).order_by(DaLeTou.cycle_num.desc())
        except:
            return jsonify(status=RET.DBERR, desc="系统繁忙")
    elif type == "排列5":
        first_cycle_num = PaiLie5.query.first().cycle_num
        last_cycle_num = PaiLie5.query.order_by(DaLeTou.cycle_num.desc()).first().cycle_num
        if start and end:
            if int(start) < int(first_cycle_num) or int(start) > int(last_cycle_num):
                return jsonify(status="1007", desc="参数错误,请输入正确的参数s")
            if int(end) > int(last_cycle_num) or int(end) < int(first_cycle_num):
                return jsonify(status="1008", desc="参数错误,请输入正确的参数e")
            if int(start) > int(end):
                return jsonify(status=RET.QUERYERR, desc="无相关数据")
            filter_params.append(and_(PaiLie5.cycle_num >= start, PaiLie5.cycle_num <= end))
        elif start:
            if int(start) < int(first_cycle_num) or int(start) > int(last_cycle_num):
                return jsonify(status="1007", desc="参数错误,请输入正确的参数s")
            filter_params.append(PaiLie5.cycle_num >= start)
        elif end:
            if int(end) > int(last_cycle_num) or int(end) < int(first_cycle_num):
                return jsonify(status="1008", desc="参数错误,请输入正确的参数e")
            filter_params.append(PaiLie5.cycle_num <= end)
        try:
            obj_query = PaiLie5.query.filter(*filter_params).order_by(PaiLie5.cycle_num.desc())
        except:
            return jsonify(status=RET.DBERR, desc="系统繁忙")
    elif type == "排列3":
        first_cycle_num = PaiLie3.query.first().cycle_num
        last_cycle_num = PaiLie3.query.order_by(DaLeTou.cycle_num.desc()).first().cycle_num

        if start and end:
            if int(start) < int(first_cycle_num) or int(start) > int(last_cycle_num):
                return jsonify(status="1007", desc="参数错误,请输入正确的参数s")
            if int(end) > int(last_cycle_num) or int(end) < int(first_cycle_num):
                return jsonify(status="1008", desc="参数错误,请输入正确的参数e")
            if int(start) > int(end):
                return jsonify(status=RET.QUERYERR, desc="无相关数据")
            filter_params.append(and_(PaiLie3.cycle_num >= start, PaiLie3.cycle_num <= end))
        elif start:
            if int(start) < int(first_cycle_num) or int(start) > int(last_cycle_num):
                return jsonify(status="1007", desc="参数错误,请输入正确的参数s")
            filter_params.append(PaiLie3.cycle_num >= start)
        elif end:
            if int(end) > int(last_cycle_num) or int(end) < int(first_cycle_num):
                return jsonify(status="1008", desc="参数错误,请输入正确的参数e")
            filter_params.append(PaiLie3.cycle_num <= end)
        try:
            obj_query = PaiLie3.query.filter(*filter_params).order_by(PaiLie3.cycle_num.desc())
        except:
            return jsonify(status=RET.DBERR, desc="系统繁忙")

    elif type == "7星彩":
        first_cycle_num = QiXingCai.query.first().cycle_num
        last_cycle_num = QiXingCai.query.order_by(DaLeTou.cycle_num.desc()).first().cycle_num
        if start and end:
            if int(start) < int(first_cycle_num) or int(start) > int(last_cycle_num):
                return jsonify(status="1007", desc="参数错误,请输入正确的参数s")
            if int(end) > int(last_cycle_num) or int(end) < int(first_cycle_num):
                return jsonify(status="1008", desc="参数错误,请输入正确的参数e")
            if int(start) > int(end):
                return jsonify(status=RET.QUERYERR, desc="无相关数据")
            filter_params.append(and_(QiXingCai.cycle_num >= start, QiXingCai.cycle_num <= end))
        elif start:
            if int(start) < int(first_cycle_num) or int(start) > int(last_cycle_num):
                return jsonify(status="1007", desc="参数错误,请输入正确的参数s")
            filter_params.append(QiXingCai.cycle_num >= start)
        elif end:
            if int(end) > int(last_cycle_num) or int(end) < int(first_cycle_num):
                return jsonify(status="1008", desc="参数错误,请输入正确的参数e")
            filter_params.append(QiXingCai.cycle_num <= end)
        try:
            obj_query = QiXingCai.query.filter(*filter_params).order_by(QiXingCai.cycle_num.desc())
        except:
            return jsonify(status=RET.DBERR, desc="系统繁忙")
    elif type == "七乐彩":
        first_cycle_num = QiLeCai.query.first().cycle_num
        last_cycle_num = QiLeCai.query.order_by(DaLeTou.cycle_num.desc()).first().cycle_num
        if start and end:
            if int(start) < int(first_cycle_num) or int(start) > int(last_cycle_num):
                return jsonify(status="1007", desc="参数错误,请输入正确的参数s")
            if int(end) > int(last_cycle_num) or int(end) < int(first_cycle_num):
                return jsonify(status="1008", desc="参数错误,请输入正确的参数e")
            if int(start) > int(end):
                return jsonify(status=RET.QUERYERR, desc="无相关数据")
            filter_params.append(and_(QiLeCai.cycle_num >= start, QiLeCai.cycle_num <= end))
        elif start:
            if int(start) < int(first_cycle_num) or int(start) > int(last_cycle_num):
                return jsonify(status="1007", desc="参数错误,请输入正确的参数s")
            filter_params.append(QiLeCai.cycle_num >= start)
        elif end:
            if int(end) > int(last_cycle_num) or int(end) < int(first_cycle_num):
                return jsonify(status="1008", desc="参数错误,请输入正确的参数e")
            filter_params.append(QiLeCai.cycle_num <= end)
        try:
            obj_query = QiLeCai.query.filter(*filter_params).order_by(QiLeCai.cycle_num.desc())
        except:
            return jsonify(status=RET.DBERR, desc="系统繁忙")
    elif type == "福彩3D":
        first_cycle_num = FuCai3d.query.first().cycle_num
        last_cycle_num = FuCai3d.query.order_by(DaLeTou.cycle_num.desc()).first().cycle_num
        if start and end:
            if int(start) < int(first_cycle_num) or int(start) > int(last_cycle_num):
                return jsonify(status="1007", desc="参数错误,请输入正确的参数s")
            if int(end) > int(last_cycle_num) or int(end) < int(first_cycle_num):
                return jsonify(status="1008", desc="参数错误,请输入正确的参数e")
            if int(start) > int(end):
                return jsonify(status=RET.QUERYERR, desc="无相关数据")
            filter_params.append(and_(FuCai3d.cycle_num >= start, FuCai3d.cycle_num <= end))
        elif start:
            if int(start) < int(first_cycle_num) or int(start) > int(last_cycle_num):
                return jsonify(status="1007", desc="参数错误,请输入正确的参数s")
            filter_params.append(FuCai3d.cycle_num >= start)
        elif end:
            if int(end) > int(last_cycle_num) or int(end) < int(first_cycle_num):
                return jsonify(status="1008", desc="参数错误,请输入正确的参数e")
            filter_params.append(FuCai3d.cycle_num <= end)
        try:
            obj_query = FuCai3d.query.filter(*filter_params).order_by(FuCai3d.cycle_num.desc())
        except:
            return jsonify(status=RET.DBERR, desc="系统繁忙")
    else:
        first_cycle_num = ShuangSeQiu.query.first().cycle_num
        last_cycle_num = ShuangSeQiu.query.order_by(DaLeTou.cycle_num.desc()).first().cycle_num

        if start and end:
            if int(start) < int(first_cycle_num) or int(start) > int(last_cycle_num):
                return jsonify(status="1007", desc="参数错误,请输入正确的参数s")
            if int(end) > int(last_cycle_num) or int(end) < int(first_cycle_num):
                return jsonify(status="1008", desc="参数错误,请输入正确的参数e")
            if int(start) > int(end):
                return jsonify(status=RET.QUERYERR, desc="无相关数据")
            filter_params.append(and_(ShuangSeQiu.cycle_num >= start, ShuangSeQiu.cycle_num <= end))
        elif start:
            if int(start) < int(first_cycle_num) or int(start) > int(last_cycle_num):
                return jsonify(status="1007", desc="参数错误,请输入正确的参数s")
            filter_params.append(ShuangSeQiu.cycle_num >= start)
        elif end:
            if int(end) > int(last_cycle_num) or int(end) < int(first_cycle_num):
                return jsonify(status="1008", desc="参数错误,请输入正确的参数e")
            filter_params.append(ShuangSeQiu.cycle_num <= end)
        try:
            obj_query = ShuangSeQiu.query.filter(*filter_params).order_by(ShuangSeQiu.cycle_num.desc())
        except:
            return jsonify(status=RET.DBERR, desc="系统繁忙")
    obj_query_page = obj_query.paginate(page, 50, False)
    obj_query_li = obj_query_page.items
    total_page = obj_query_page.pages
    if total_page == 0:
        return jsonify(status=RET.QUERYERR, desc="无相关数据")
    if page > total_page:
        return jsonify(status=RET.PARAMERR, desc="参数错误,请输入正确的参数page")

    num = obj_query_page.total
    # num=len(obj_query)
    #
    # total_page=math.ceil(num/50)
    #
    # obj_query=[obj_query[i:i + 50] for i in range(0, num, 50)]
    # first_obj = obj_query.paginate(1, 50, False).items[0]
    # s_cycle_num = first_obj.cycle_num
    # last_obj = obj_query.paginate(4, 50, False).items[-1]
    # l_cycle_num = last_obj.cycle_num

    queryinfo = []

    for i in obj_query_li:
        queryinfo.append(i.to_full_dict())

    resp = dict(status=RET.OK, desc="查询成功",
                result={"queryinfo": queryinfo, "total_page": min(total_page, 4), "current_page": page,
                        "num": min(num, 200)})
    return jsonify(resp), 200, {"Content-Type": "application/json"}
