from . import db
from datetime import datetime, date
import json


class City(db.Model):
    __tablename__ = 'lianjia_city'
    id = db.Column(db.Integer, primary_key=True)
    city = db.Column(db.String(8), nullable=False)


class House(db.Model):
    __tablename__ = 'lianjia_house'
    id = db.Column(db.Integer, primary_key=True)
    city = db.Column(db.String(50), default="")
    house_name = db.Column(db.String(50), default="")
    detail_url = db.Column(db.String(1000), default="")
    area = db.Column(db.String(30), default="")
    area_tab = db.Column(db.String(30), default="")
    address = db.Column(db.String(500), default="")
    price = db.Column(db.Integer)
    total_price = db.Column(db.Integer)
    house_area = db.Column(db.String(20), default="")
    house_trait = db.Column(db.String(100), default="")
    house_shape = db.Column(db.String(30), default="")
    open_time = db.Column(db.String(25), default="")
    service_type = db.Column(db.String(16), default="")
    sale_status = db.Column(db.String(18), default="")
    developers = db.Column(db.String(50), default="")
    sale_address = db.Column(db.String(100), default="")
    house_img = db.Column(db.String(256), default="")
    build_type = db.Column(db.String(86), default="")
    total_space = db.Column(db.BigInteger)
    build_space = db.Column(db.BigInteger)
    total_floors = db.Column(db.Integer)
    house_years = db.Column(db.Integer)
    greening_rate = db.Column(db.String(16), default="")
    volume_rate = db.Column(db.String(16), default="")
    property_company = db.Column(db.String(100), default="")
    property_fee = db.Column(db.String(32), default="")
    water_mode = db.Column(db.String(12), default="")
    parking_lot = db.Column(db.String(500), default="")
    parking_ratio = db.Column(db.String(56), default="")
    heating_mode = db.Column(db.String(16), default="")
    power_supply_mode = db.Column(db.String(16), default="")
    is_delete = db.Column(db.Integer, default=0)
    is_update = db.Column(db.Integer, default=0)

    def to_basic_dict(self):
        """将基本信息转换为字典数据"""
        house_dict = {
            "house_id": self.id,
            "city": self.city,
            "house_name": self.house_name,
            "area": self.area,
            "area_tab": self.area_tab,
            "address": self.address,
            "price": self.price,
            "total_price": self.total_price,
            "house_area": self.house_area,
            "service_type": self.service_type,
            "sale_status": self.sale_status,
            "house_img": self.house_img,
            "house_trait": self.house_trait,
            "house_shape": self.house_shape
        }

        return house_dict

    def to_full_dict(self):
        house_dict = {
            "house_id": self.id,
            "city": self.city,
            "house_name": self.house_name,
            "area": self.area,
            "area_tab": self.area_tab,
            "address": self.address,
            "price": self.price,
            "build_type": self.build_type,
            "service_type": self.service_type,
            "sale_status": self.sale_status,
            "build_space": self.build_space,
            "total_space": self.total_space,
            "house_trait": self.house_trait,
            "open_time": self.open_time,
            "developers": self.developers,

        }
        img_urls = []
        total_imgs = HouseImg.query.filter_by(city=self.city, house_name=self.house_name,
                                              service_type=self.service_type, detail_url=self.detail_url).all()
        for i in total_imgs:
            img_urls.append(i.img_url.split(",")[0].replace("235x178", "750x380"))
        house_dict["img_urls"] = img_urls

        dynamic_infos = []
        total_dynamics = HouseDynamic.query.filter_by(city=self.city, house_name=self.house_name,
                                                      service_type=self.service_type,
                                                      detail_url=self.detail_url).order_by(
            HouseDynamic.push_time.desc()).limit(3)
        # print(total_dynamics)
        for i in total_dynamics:
            dynamic_infos.append(
                {"dynamic_content": i.dynamic_content, "dynamic_type": i.dynamic_type, 'push_time': str(i.push_time),
                 "dynamic_title": i.dynamic_title})
        house_dict["dynamic_infos"] = dynamic_infos

        return house_dict


class HouseDynamic(db.Model):
    __tablename__ = 'lianjia_house_dynamic'
    id = db.Column(db.Integer, primary_key=True)
    city = db.Column(db.String(50), default="")
    house_name = db.Column(db.String(50), default="")
    detail_url = db.Column(db.String(1000), default="")
    service_type = db.Column(db.String(16), default="")
    dynamic_type = db.Column(db.String(132), default="")
    dynamic_title = db.Column(db.String(1000), default="")
    push_time = db.Column(db.Date)
    dynamic_content = db.Column(db.TEXT, default="")
    is_delete = db.Column(db.Integer, default=0)


class HouseImg(db.Model):
    __tablename__ = 'lianjia_house_img'
    id = db.Column(db.Integer, primary_key=True)
    city = db.Column(db.String(50), default="")
    house_name = db.Column(db.String(50), default="")
    detail_url = db.Column(db.String(1000), default="")
    service_type = db.Column(db.String(16), default="")
    img_type = db.Column(db.String(130), default="")
    img_num = db.Column(db.Integer, default=0)
    img_url = db.Column(db.TEXT, default="")
    is_delete = db.Column(db.Integer, default=0)


class HouseShape1(db.Model):
    __tablename__ = 'lianjia_house_shape'
    id = db.Column(db.Integer, primary_key=True)
    city = db.Column(db.String(50), default="")
    house_name = db.Column(db.String(50), default="")
    detail_url = db.Column(db.String(1000), default="")
    service_type = db.Column(db.String(16), default="")
    house_shape_name = db.Column(db.String(160), default="")
    house_type = db.Column(db.String(140), default="")
    avg_price = db.Column(db.String(156), default="")
    house_shape_feature = db.Column(db.String(256), default="")
    house_describe = db.Column(db.TEXT, default="")
    new_sale_type = db.Column(db.String(140), default="")
    house_shape_img = db.Column(db.String(500), default="")
    is_delete = db.Column(db.Integer, default=0)


class HouseInfo1(db.Model):
    __tablename__ = 'lianjia_house_info'
    id = db.Column(db.Integer, primary_key=True)
    city = db.Column(db.String(50), default="")
    house_name = db.Column(db.String(50), default="")
    detail_url = db.Column(db.String(1000), default="")
    service_type = db.Column(db.String(16), default="")
    new_open_time = db.Column(db.String(256), default="")
    sale_num = db.Column(db.TEXT)
    done_house_time = db.Column(db.String(256), default="")
    is_delete = db.Column(db.Integer, default=0)


class HouseCertificate1(db.Model):
    __tablename__ = 'lianjia_house_certificate'
    id = db.Column(db.Integer, primary_key=True)
    city = db.Column(db.String(50), default="")
    house_name = db.Column(db.String(50), default="")
    detail_url = db.Column(db.String(1000), default="")
    service_type = db.Column(db.String(16), default="")
    licence = db.Column(db.String(512), default="")
    bind_floor = db.Column(db.TEXT, default="")
    licence_time = db.Column(db.String(256), default="")
    is_delete = db.Column(db.Integer, default=0)


class Area(db.Model):
    __tablename__ = 'lianjia_city_area'
    id = db.Column(db.Integer, primary_key=True)
    city_id = db.Column(db.Integer, db.ForeignKey("lianjia_city.id"), nullable=False)
    area_name = db.Column(db.String(56), default="")


class BaseModel(object):
    is_delete = db.Column(db.Integer, default=0)


class NationalDetail(db.Model, BaseModel):
    __tablename__ = 'kj500_national_detaile'
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(16))
    cycle_num = db.Column(db.String(50))
    open_time = db.Column(db.String(50))
    close_time = db.Column(db.String(50))
    winning_number = db.Column(db.String(50))
    order = db.Column(db.String(50))
    week = db.Column(db.String(50))
    sales_volume = db.Column(db.String(50))
    jackpot = db.Column(db.String(50))
    open_detail = db.Column(db.TEXT)
    test_num = db.Column(db.String(50))

    def to_base_dict(self):
        kj_dict = {
            'type': self.type,
            'cycle_num': self.cycle_num,
            'open_time': self.open_time,
            'winning_number': self.winning_number,
            'order': self.order,
            'week': self.week,
        }
        if self.test_num:
            kj_dict['test_num'] = self.test_num

        return kj_dict

    def to_detail_dict(self):
        if self.type == "超级大乐透":
            a, b, c, d, e, f = [], [], [], [], [], []
            for i in json.loads(self.open_detail):

                if i["award"] == '一等奖':
                    i.pop("award")
                    a.append(i)
                elif i["award"] == '二等奖':
                    i.pop("award")
                    b.append(i)
                elif i["award"] == '三等奖':
                    i.pop("award")
                    c.append(i)
                elif i["award"] == '四等奖':
                    i.pop("award")
                    d.append(i)
                elif i["award"] == '五等奖':
                    i.pop("award")
                    e.append(i)
                elif i["award"] == '六等奖':
                    i.pop("award")
                    f.append(i)
            content_list = []
            content_list.append({"award": '一等奖', "array": a})
            content_list.append({"award": '二等奖', "array": b})
            content_list.append({"award": '三等奖', "array": c})
            content_list.append({"award": '四等奖', "array": d})
            content_list.append({"award": '五等奖', "array": e})
            content_list.append({"award": '六等奖', "array": f})

            kj_dict = {
                'open_time': self.open_time,
                'close_time': self.close_time,
                'sales_volume': self.sales_volume,
                'jackpot': self.jackpot,
                'open_detail': content_list
            }
        elif self.type == "福彩3D":
            a, b, c, d = [], [], [], []
            content_list = []
            for i in json.loads(self.open_detail):
                if i["award"] == '猜1D':
                    i.pop("award")
                    a.append(i)
                elif i["award"] == '猜2D':
                    i.pop("award")
                    b.append(i)
                elif i["award"] == '通选':
                    i.pop("award")
                    c.append(i)
                elif i["award"] == '包选':
                    i.pop("award")
                    d.append(i)
                else:
                    content_list.append(i)
            content_list.append({"award": '猜1D', "array": a})
            content_list.append({"award": '猜2D', "array": b})
            content_list.append({"award": '通选', "array": c})
            content_list.append({"award": '包选', "array": d})

            kj_dict = {
                'open_time': self.open_time,
                'close_time': self.close_time,
                'sales_volume': self.sales_volume,
                'jackpot': self.jackpot,
                'open_detail': content_list,
                'test_num': self.test_num

            }



        else:
            kj_dict = {
                'open_time': self.open_time,
                'close_time': self.close_time,
                'sales_volume': self.sales_volume,
                'jackpot': self.jackpot,
                'open_detail': json.loads(self.open_detail)
            }
        return kj_dict

    def to_previous_dict(self):
        if self.type == "超级大乐透":
            a, b, c, d, e, f = [], [], [], [], [], []
            for i in json.loads(self.open_detail):

                if i["award"] == '一等奖':
                    i.pop("award")
                    a.append(i)
                elif i["award"] == '二等奖':
                    i.pop("award")
                    b.append(i)
                elif i["award"] == '三等奖':
                    i.pop("award")
                    c.append(i)
                elif i["award"] == '四等奖':
                    i.pop("award")
                    d.append(i)
                elif i["award"] == '五等奖':
                    i.pop("award")
                    e.append(i)
                elif i["award"] == '六等奖':
                    i.pop("award")
                    f.append(i)
            content_list = []
            content_list.append({"award": '一等奖', "array": a})
            content_list.append({"award": '二等奖', "array": b})
            content_list.append({"award": '三等奖', "array": c})
            content_list.append({"award": '四等奖', "array": d})
            content_list.append({"award": '五等奖', "array": e})
            content_list.append({"award": '六等奖', "array": f})

            kj_dict = {
                'cycle_num': self.cycle_num,
                'open_time': self.open_time,
                'winning_number': self.winning_number,
                'order': self.order,
                'close_time': self.close_time,
                'sales_volume': self.sales_volume,
                'jackpot': self.jackpot,
                'open_detail': content_list
            }
        elif self.type == "福彩3D":
            a, b, c, d = [], [], [], []
            content_list = []
            for i in json.loads(self.open_detail):
                if i["award"] == '猜1D':
                    i.pop("award")
                    a.append(i)
                elif i["award"] == '猜2D':
                    i.pop("award")
                    b.append(i)
                elif i["award"] == '通选':
                    i.pop("award")
                    c.append(i)
                elif i["award"] == '包选':
                    i.pop("award")
                    d.append(i)
                else:
                    content_list.append(i)
            content_list.append({"award": '猜1D', "array": a})
            content_list.append({"award": '猜2D', "array": b})
            content_list.append({"award": '通选', "array": c})
            content_list.append({"award": '包选', "array": d})

            kj_dict = {
                'cycle_num': self.cycle_num,
                'open_time': self.open_time,
                'winning_number': self.winning_number,
                'order': self.order,
                'close_time': self.close_time,
                'sales_volume': self.sales_volume,
                'jackpot': self.jackpot,
                'open_detail': content_list,
                'test_num': self.test_num

            }
        else:
            kj_dict = {
                'cycle_num': self.cycle_num,
                'open_time': self.open_time,
                'winning_number': self.winning_number,
                'order': self.order,
                'close_time': self.close_time,
                'sales_volume': self.sales_volume,
                'jackpot': self.jackpot,
                'open_detail': json.loads(self.open_detail)
            }
        return kj_dict


class ShuangSeQiu(db.Model):
    __tablename__ = 'kj500_shuangseqiu'
    id = db.Column(db.Integer, primary_key=True)
    cycle_num = db.Column(db.String(30))
    winning_number = db.Column(db.String(256))

    def to_full_dict(self):
        kj_dict = {
            'cycle_num': self.cycle_num.strip() + "期",
            'open_time': None,
            'winning_number': ",".join(json.loads(self.winning_number)),
            'week': None,

        }
        return kj_dict


class DaLeTou(db.Model):
    __tablename__ = 'kj500_daletou'
    id = db.Column(db.Integer, primary_key=True)
    cycle_num = db.Column(db.String(30))
    winning_number = db.Column(db.String(256))

    def to_full_dict(self):
        kj_dict = {
            'cycle_num': self.cycle_num.strip() + "期",
            'open_time': None,
            'winning_number': ",".join(json.loads(self.winning_number)),
            'week': None,
        }
        return kj_dict


class PaiLie3(db.Model):
    __tablename__ = 'kj500_pailie3'
    id = db.Column(db.Integer, primary_key=True)
    cycle_num = db.Column(db.String(30))
    open_time = db.Column(db.Date)
    winning_number = db.Column(db.String(256))

    def get_week_day(self, date):
        week_day_dict = {
            0: '星期一',
            1: '星期二',
            2: '星期三',
            3: '星期四',
            4: '星期五',
            5: '星期六',
            6: '星期天',
        }
        day = date.weekday()
        return week_day_dict[day]

    def to_full_dict(self):
        kj_dict = {
            'cycle_num': self.cycle_num.strip() + "期",
            'open_time': str(self.open_time).split('-')[0] + "年" + str(self.open_time).split('-')[1] + "月" +
                         str(self.open_time).split('-')[2] + "日",
            'winning_number': ",".join(json.loads(self.winning_number)),
            'week': self.get_week_day(self.open_time),
        }
        return kj_dict


class PaiLie5(db.Model):
    __tablename__ = 'kj500_pailie5'
    id = db.Column(db.Integer, primary_key=True)
    cycle_num = db.Column(db.String(30))
    winning_number = db.Column(db.String(256))

    def to_full_dict(self):
        kj_dict = {
            'cycle_num': self.cycle_num.strip() + "期",
            'open_time': None,
            'winning_number': ",".join(json.loads(self.winning_number)),
            'week': None,
        }
        return kj_dict


class QiXingCai(db.Model):
    __tablename__ = 'kj500_qixingcai'
    id = db.Column(db.Integer, primary_key=True)
    cycle_num = db.Column(db.String(30))
    winning_number = db.Column(db.String(256))

    def to_full_dict(self):
        kj_dict = {
            'cycle_num': self.cycle_num.strip() + "期",
            'open_time': None,
            'winning_number': ",".join(json.loads(self.winning_number)),
            'week': None,
        }
        return kj_dict


class FuCai3d(db.Model):
    __tablename__ = 'kj500_fucai3d'
    id = db.Column(db.Integer, primary_key=True)
    cycle_num = db.Column(db.String(30))
    open_time = db.Column(db.Date)
    winning_number = db.Column(db.String(256))

    def get_week_day(self, date):
        week_day_dict = {
            0: '星期一',
            1: '星期二',
            2: '星期三',
            3: '星期四',
            4: '星期五',
            5: '星期六',
            6: '星期天',
        }
        day = date.weekday()
        return week_day_dict[day]

    def to_full_dict(self):
        kj_dict = {
            'cycle_num': self.cycle_num.strip() + "期",
            'open_time': str(self.open_time).split('-')[0] + "年" + str(self.open_time).split('-')[1] + "月" +
                         str(self.open_time).split('-')[2] + "日",
            'winning_number': ",".join(json.loads(self.winning_number)),
            'week': self.get_week_day(self.open_time),
        }
        return kj_dict


class QiLeCai(db.Model):
    __tablename__ = 'kj500_qilecai'
    id = db.Column(db.Integer, primary_key=True)
    cycle_num = db.Column(db.String(30))
    winning_number = db.Column(db.String(256))

    def to_full_dict(self):
        kj_dict = {
            'cycle_num': self.cycle_num.strip() + "期",
            'open_time': None,
            'winning_number': ",".join(json.loads(self.winning_number)),
            'week': None,
        }
        return kj_dict


class CarInfo(db.Model):
    __tablename__ = 'carhome_cartype'
    id = db.Column(db.Integer, primary_key=True)
    brand_name = db.Column(db.String(56))
    system_type = db.Column(db.String(56))
    system_name = db.Column(db.String(56))
    sale_type = db.Column(db.String(20))
    car_name = db.Column(db.String(256))

    def to_full_dict(self):
        car_dict = {
            "id": self.id,
            "brand_name": self.brand_name,
            "system_type": self.system_type,
            "system_name": self.system_name,
            "sale_type": self.sale_type,
            "car_name": self.car_name,
        }
        return car_dict


class CarDetaile(db.Model):
    __tablename__ = 'carhome_car_detaile'
    id = db.Column(db.Integer, primary_key=True)
    brand_name = db.Column(db.String(56))
    system_name = db.Column(db.String(56))
    car_name = db.Column(db.String(256))
    car_type = db.Column(db.String(100))
    dealer_price = db.Column(db.String(56))
    factory_price = db.Column(db.String(56))
    driving_mode = db.Column(db.String(56))
    gear_box = db.Column(db.String(50))
    car_structure = db.Column(db.String(256))
    motor = db.Column(db.String(256))
    car_size = db.Column(db.String(256))

    def to_full_dict(self):
        car_dict = {
            "brand_name": self.brand_name,
            "system_name": self.system_name,
            "car_name": self.car_name,
            "car_type": self.car_type,
            "dealer_price": self.dealer_price,
            "factory_price": self.factory_price,
            "driving_mode": self.driving_mode,
            "gear_box": self.gear_box,
            "car_structure": self.car_structure,
            "motor": self.motor,
            "car_size": self.car_size,
        }

        return car_dict


class EnergyDetaile(db.Model):
    __tablename__ = 'carhome_energy_detaile'
    id = db.Column(db.Integer, primary_key=True)
    brand_name = db.Column(db.String(56))
    system_name = db.Column(db.String(56))
    car_name = db.Column(db.String(256))
    energy_type = db.Column(db.String(100))
    renewal = db.Column(db.String(156))
    slow_charge = db.Column(db.String(156))
    fast_charge = db.Column(db.String(156))
    battery_capacity = db.Column(db.String(150))
    fast_percentage = db.Column(db.String(256))
    warranty = db.Column(db.String(256))
    car_size = db.Column(db.String(256))
    factory_price = db.Column(db.String(256))
    dealer_price = db.Column(db.String(56))

    def to_full_dict(self):
        car_dict = {
            "brand_name": self.brand_name,
            "system_name": self.system_name,
            "car_name": self.car_name,
            "energy_type": self.energy_type,
            "renewal": self.renewal,
            "slow_charge": self.slow_charge,
            "fast_charge": self.fast_charge,
            "battery_capacity": self.battery_capacity,
            "fast_percentage": self.fast_percentage,
            "warranty": self.warranty,
            "car_size": self.car_size,
            "factory_price": self.factory_price,
            "dealer_price": self.dealer_price,
        }

        return car_dict


class ExchangeRateModel(db.Model):
    __tablename__ = 'exchange_rate'
    id = db.Column(db.Integer, primary_key=True)
    currency = db.Column(db.String(66))
    price_type = db.Column(db.String(66))
    zg_bank = db.Column(db.Float)
    gs_bank = db.Column(db.Float)
    jt_bank = db.Column(db.Float)
    ny_bank = db.Column(db.Float)
    pf_bank = db.Column(db.Float)
    js_bank = db.Column(db.Float)
    zs_bank = db.Column(db.Float)
    gd_bank = db.Column(db.Float)
    is_delete = db.Column(db.Integer, default=0)
    create_date = db.Column(db.DateTime, default=datetime.now)

    def to_full_dict(self):
        rate_dict = {
            "currency": self.currency,
            "price_type": self.price_type,
            "zg_bank": str(self.zg_bank),
            "gs_bank": str(self.gs_bank),
            "jt_bank": str(self.jt_bank),
            "ny_bank": str(self.ny_bank),
            "pf_bank": str(self.pf_bank),
            "js_bank": str(self.js_bank),
            "zs_bank": str(self.zs_bank),
            "gd_bank": str(self.gd_bank),
            "create_date": str(self.create_date),
        }
        return rate_dict


class YouJia(db.Model):
    __tablename__ = 'youjia'
    id = db.Column(db.Integer, primary_key=True)
    area = db.Column(db.String(16))
    type_89 = db.Column(db.Float)
    type_92 = db.Column(db.Float)
    type_95 = db.Column(db.Float)
    type_0 = db.Column(db.Float)
    update_time = db.Column(db.DateTime)
    is_delete = db.Column(db.Integer, default=0)

    def to_full_dict(self):
        youjia_dict = {
            "area": self.area,
            "type_89": str(self.type_89),
            "type_92": str(self.type_92),
            "type_95": str(self.type_95),
            "type_0": str(self.type_0),
            "update_time": str(self.update_time),

        }
        return youjia_dict


class GoldModel(db.Model):
    __tablename__ = 'gold_silver_price'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(256))
    publish_time = db.Column(db.Date)
    contract = db.Column(db.String(256))
    open_price = db.Column(db.String(126))
    highest_price = db.Column(db.String(126))
    minimum_price = db.Column(db.String(126))
    closing_price = db.Column(db.String(126))
    up_down = db.Column(db.String(126))
    up_down_range = db.Column(db.String(36))
    average_price = db.Column(db.String(126))
    volume = db.Column(db.String(126))
    transaction_amount = db.Column(db.String(126))
    open_interest = db.Column(db.String(126))
    payment_direction = db.Column(db.String(126))
    Yield = db.Column(db.String(126))

    def to_part_dict(self):
        gold_dict = {
            "title": self.title,
            "publish_time": str(self.publish_time),

        }
        return gold_dict

    def to_full_dict(self):
        gold_dict = {
            "contract": self.contract,
            "open_price": str(self.open_price),
            "highest_price": str(self.highest_price),
            "minimum_price": str(self.minimum_price),
            "closing_price": str(self.closing_price),
            "up_down": str(self.up_down),
            "up_down_range": self.up_down_range,
            "average_price": str(self.average_price),
            "volume": str(self.volume),
            "transaction_amount": self.transaction_amount,
            "open_interest": self.open_interest,
            "payment_direction": self.payment_direction,
            "yield": self.Yield,
            "publish_time": str(self.publish_time)
        }
        return gold_dict


class QiuBaiWord(db.Model):
    __tablename__ = 'qiubai_words'
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.TEXT)


class QiuBaiImg(db.Model):
    __tablename__ = 'qiubai_imgs'
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.TEXT)
    img = db.Column(db.String(1024))


class ShiShiModel(db.Model):
    __tablename__ = 'piaofang_shishi'
    id = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.Date, default=date.today())
    update_time = db.Column(db.String(256))
    MovieName = db.Column(db.String(50))  # 电影名称
    Irank = db.Column(db.Integer)  # 票房排名
    BoxOffice = db.Column(db.Float)  # 实时票房(万)
    sumBoxOffice = db.Column(db.Float)  # 累计票房（万）
    movieDay = db.Column(db.String(256))  # 上映天数
    boxPer = db.Column(db.String(50))  # 票房占比
    is_delete = db.Column(db.Integer, default=0)

    def to_base_dict(self):
        shishi_dict = {
            "MovieName": self.MovieName,
            "Irank": self.Irank,
            "BoxOffice": self.BoxOffice,
            "sumBoxOffice": self.sumBoxOffice,
            "movieDay": self.movieDay,
            "boxPer": self.boxPer,
            "update_time": self.update_time,
            "datetime": str(self.datetime),
        }
        return shishi_dict


class SingleModel(db.Model):
    __tablename__ = 'piaofang_danri'
    id = db.Column(db.Integer, primary_key=True)
    piaofang_date = db.Column(db.String(500))
    MovieName = db.Column(db.String(50))  # 电影名称
    IRank = db.Column(db.Integer)  # 票房排名
    BoxOffice = db.Column(db.Float)  # 实时票房(万)
    SumBoxOffice = db.Column(db.Float)  # 累计票房（万）
    MovieDay = db.Column(db.String(256))  # 上映天数
    AvgPrice = db.Column(db.Integer)
    AvpPeoPle = db.Column(db.Integer)
    WomIndex = db.Column(db.String(66))
    BoxOffice_Up = db.Column(db.String(66))
    is_delete = db.Column(db.Integer, default=0)

    def to_full_dict(self):
        day_dict = {
            "MovieName": self.MovieName,
            "IRank": self.IRank,
            "BoxOffice": self.BoxOffice,
            "SumBoxOffice": self.SumBoxOffice,
            "MovieDay": self.MovieDay,
            "AvgPrice": self.AvgPrice,
            "AvpPeoPle": self.AvpPeoPle,
            "WomIndex": self.WomIndex,
            "BoxOffice_Up": self.BoxOffice_Up,
        }
        return day_dict


class ElseModel(db.Model):
    __tablename__ = 'piaofang_else'
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(166))
    piaofang_date = db.Column(db.String(266))
    data = db.Column(db.TEXT)
